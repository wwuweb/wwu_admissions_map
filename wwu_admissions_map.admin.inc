<?php

/**
 * Implements hook_form().
 */
function wwu_admissions_map_form() {
  $form = array();

  # Note: for variable_get, the second parameter is a default_value
  $form['wwu_admissions_map_view_country'] = array(
      '#type' => 'textfield',
      '#title' => 'State counselors data view path',
      '#description' => 'The path to the JSON view of state counselor data.',
      '#default_value' => variable_get('wwu_admissions_map_view_country', '/counselors'),
      '#size' => 30,
      '#maxlength' => 255,
      '#required' => TRUE
  );

  $form['wwu_admissions_map_view_washington'] = array(
      '#type' => 'textfield',
      '#title' => 'Washington counselors data view path',
      '#description' => 'The path to the JSON view of Washington counselor data.',
      '#default_value' => variable_get('wwu_admissions_map_view_washington', '/counselors'),
      '#size' => 30,
      '#maxlength' => 255,
      '#required' => TRUE
  );

  return system_settings_form($form);
}

/**
 * Implements hook_form_validate().
 */
function wwu_admissions_map_form_validate($form, &$form_state) {
  if ($form == 'wwu_admissions_map_form') {
    $form_state['wwu_admissions_map_view_country'] = check_plain($form_state['wwu_admissions_map_view_country']);
    $form_state['wwu_admissions_map_view_washington'] = check_plain($form_state['wwu_admissions_map_view_washington']);
  }
}

