/**
 * @file wwu-admissions-map.js
 * @author Nigel Packer : WebTech : Western Washington University
 *
 * Insert a dynamically generated svg map into the templated container element.
 * Load additional JSON data via AJAX to supply enrichment on interaction.
 */

(function ($, Drupal, window, document, undefined) {

  var counselorsJSON,
      timeout;

  /**
   * Apply a toggleable click event handler to an element with a mandatory
   * callback function that is executed when the button is clicked.
   *
   * @param {String}
   *          selector a valid selector for the target element
   * @param {Function}
   *          callback the callback function to be called when the button is
   *          clicked
   */
  function button(selector, callback) {
    var button;

    button = $(selector)
      .click(function () {
        button.unbind('click').hide();
        callback();
      }).show();
  }

  /**
   * Called on the click event for a SVG feature. First checks the region of the
   * feature to determine whether or not the type of feature is a Washington
   * county. Otherwise, it checks if the features is Washington itself, which
   * invokes special behavior (i.e. loading the Washington map).
   *
   * @param {Object}
   *          d the d3 data binding object for the active feature
   * @param {Object}
   *          path the d3 path object to be applied
   * @param {Object}
   *          svg the parent SVG element for the clicked feature
   */
  function clickFeature(d, path, svg) {
    var active;

    active = d3.select("path.active");
    active.classed("active", false);

    if (active.node() === d3.event.target) {
      resetZoom(svg);
    } else {
      active = d3.select(d3.event.target);
      active.classed("active", true);

      zoomToFeature(d, path, svg, function () {
        var counselors;

        if (d.properties.name === "Washington") {
          svg.transition()
            .duration(timeout)
            .style("opacity", "0")
            .each("end", function () {
              svg.remove();
              washingtonCounselors();
            });
        } else {
          counselors = [];

          if (d.properties.region === "Washington"
              || d.properties.region === "west") {
            counselors = getCounselors(d.properties.name);
          } else {
            counselors = getCounselors(d.properties.region);
          }

          displayCounselors(counselors);
        }
      });
    }
  }

  /**
   * Display the array of counselor data in the browser.
   *
   * @param {Object[]}
   *          counselors the array of counselor JSON objects
   */
  function displayCounselors(counselors) {
    var background,
        container,
        counselor,
        overlay;

    if (counselors.length > 0) {
      container = $('.wwu-admissions-overlay-container')
        .empty();
      background = $('.wwu-admissions-overlay-background')
        .click(function () {
          background.unbind().fadeOut();
          container.fadeOut();
        });

      while (counselor = counselors.pop()) {
        overlay = getOverlayTemplate();
        overlay.find('.wwu-admissions-overlay-name-content')
          .text(counselor.name);
        overlay.find('.wwu-admissions-overlay-phone-content')
          .text(counselor.phone);
        overlay.find('.wwu-admissions-overlay-email-content')
          .attr('href', 'mailto:' + counselor.email)
          .text(counselor.email);
        overlay.find('.wwu-admissions-overlay-image-content')
          .attr('src', counselor.image.src);
        overlay.find('.wwu-admissions-overlay-profile-content')
          .attr('href', counselor.profile)
          .text('Get to know ' + counselor.name + ' better...');
        overlay.appendTo(container);
      }

      background.fadeIn();
      container.fadeIn();
    }
  }

  /**
   * Display a visible error message in the browser when a JSON file fails to
   * load.
   *
   * @param {String}
   *          file the file name or path to the document that failed to load
   */
  function displayJsonError(file) {
    displayRuntimeError("The JSON file \"" + file + "\" failed to load.");
  }

  /**
   * Display a general error message to the user with the given text.
   *
   * @param {String}
   *          message error message text
   */
  function displayRuntimeError(message) {
    $("#wwu-admissions-error").text(message).show();
  }

  /**
   * Get the width of the first parent element of the selected element with a
   * non-zero width. If none is found, return 1000.
   *
   * @param {String}
   *          selector the selector of the element for which to find parents
   */
  function getContainerWidth(selector) {
    var container,
        width;

    container = $(selector).parent();
    width = container.width();

    while (width === 0) {
      container = container.parent();
      width = container.width();
    }

    if (!width) {
      width = 1000;
    }

    return width;
  }

  /**
   * Get a jQuery object clone of the counselors information overlay template.
   *
   * @returns {Object} jQuery deep clone of overlay tempalte HTML.
   */
  function getOverlayTemplate() {
    return $('#wwu-admissions-overlay-template').clone().removeAttr('id');
  }

  /**
   * Scan the "counselors" JSON object for the name of the geographic region
   * compared to the name of the currently active feature.
   *
   * @param {String}
   *          featureName the name of the feature to match
   *
   * @returns {String[]} the array of counselor identifiers
   */
  function getCounselors(featureName) {
    var counselors;

    counselors = [];

    for (var key in counselorsJSON) {
      if (counselorsJSON[key].region === featureName) {
        counselors.push(counselorsJSON[key]);
      }
    }

    return counselors;
  }

  /**
   * Get the top-level keys of an object.
   *
   * @param {Object}
   *          object the object to scan for parameters
   *
   * @returns {String[]} the array of keys
   */
  function getObjectKeys(object) {
    var keys;

    keys = [];

    for (var key in object) {
      keys[keys.length] = key;
    }

    return keys;
  }

  /**
   * Calculate the scale for a d3 projection based on a given width of a
   * container element, modified by a constant factor.
   *
   * @param {Number}
   *          width the width of the container element
   * @param {Number}
   *          factor a scalar modifier with which to adjust the calculated value
   *
   * @returns {Number} the calculated scale
   */
  function getScale(width, factor) {
    return width * factor;
  }

  /**
   * Reset the transform attribute of a given svg element (requires that the
   * transformable elements be contained in a group tag (g).
   *
   * @param {Object}
   *          svg the SVG to transform
   */
  function resetZoom(svg) {
    svg.select("g").transition()
      .duration(timeout)
      .style("stroke-width", "2px")
      .attr("transform", "");
  }

  /**
   * Add the overlay background and container to the DOM in the appropriate
   * locations.
   */
  function setupOverlay() {
    $('<div class="wwu-admissions-overlay-container"></div>').prependTo('body');
    $('<div class="wwu-admissions-overlay-background"></div>').prependTo('body');
  }

  /**
   * Generate a SVG with d3 from a TopoJSON file. jQuery is used to fetch the
   * JSON source. Appends it to the element determined by the given selector.
   *
   * @param {Object}
   *          options information about the svg to be created
   * @param {String}
   *          options.id the id to apply to the svg
   * @param {String}
   *          options.source the path to the TopoJSON file to be loaded
   * @param {String}
   *          options.parent the selector for the element to which the SVG will
   *          be appended
   * @param {Number}
   *          options.width the width of the SVG
   * @param {Number}
   *          options.height the height of the SVG
   * @param {Ojbect}
   *          options.path the d3 path object to apply to the features contained
   *          in the SVG
   * @param {Function}
   *          [options.callback] a callback function to apply additional
   *          attributes and events to the features of the SVG
   *
   * @returns {Object} the generated SVG as a d3 container object
   */
  function svg(options) {
    var svg;

    svg = d3.select(options.parent).append("svg")
      .attr("id", options.id)
      .attr("width", options.width)
      .attr("height", options.height);

    $.getJSON(options.source, function (data) {
      var paths,
          featuresKey;

      featuresKey = getObjectKeys(data.objects).shift();

      paths = svg.append("g")
        .selectAll("path")
          .data(topojson.feature(data, data.objects[featuresKey]).features)
          .enter().append("path")
            .attr("d", options.path);

      if (typeof options.callback === 'function') {
        options.callback(paths);
      }
    })
      .fail(function (jqXHR, textStatus, error) {
        displayJsonError(options.source);
      });

    return svg;
  }

  /**
   * Display counselors for the US by state.
   */
  function usCounselors() {
    var factor,
        height,
        path,
        projection,
        us,
        width;

    factor = 1.2;
    width = getContainerWidth("#wwu-admissions-map");
    height = Math.ceil(0.65 * width);

    projection = d3.geo.albersUsa()
      .scale(getScale(width, factor))
      .translate([ width / 2, height / 2 ]);

    path = d3.geo.path()
      .projection(projection);

    us = svg({
      id: "wwu-us-admissions-map",
      source: Drupal.settings.wwu_admissions_map.us,
      parent: "#wwu-admissions-map",
      width: width,
      height: height,
      path: path,
      callback: function (paths) {
        paths
          .attr("id", function (d) { return d.properties.name; })
          .attr("class", function (d) { return "state " + d.properties.region; })
          .on("click", function (d) { clickFeature(d, path, us); });
      }
    });
  }

  /**
   * Check that the correct keys and data types are provided by the counselors
   * JSON view. If errors are found, attempt to provide guidance to correct.
   *
   * @param {Object}
   *          data the JSON data object
   *
   * @param {String[]}
   *          requiredKeys an array of required keys
   *
   * @throws {Error}
   *           if a key (field) is not found in the given dataset.
   */
  function validateViewsJson(data, requiredKeys) {
    var keys;

    keys = getObjectKeys(data[0]);

    for (var i = requiredKeys.length; i--;) {
      var key;

      key = requiredKeys[i];

      if (keys.indexOf(key) < 0) {
        throw "The field '"
            + key
            + "' was not found in the JSON view '"
            + Drupal.settings.wwu_admissions_map.counselors
            + "' required by this module. Ensure that the view is present and configured correctly.";
      }
    }
  }

  /**
   * Display counselors for Washington state by county. Display a click-enabled
   * button to allow the user to navigate back to the US counselors map.
   */
  function washingtonCounselors() {
    var factor,
        height,
        path,
        projection,
        wa,
        width;

    factor = 6;
    width = getContainerWidth("#wwu-admissions-map");
    height = Math.ceil(0.65 * width);

    projection = d3.geo.mercator()
      .center([-120.5, 47.5])
      .scale(getScale(width, factor))
      .translate([width / 2, height / 2]);

    path = d3.geo.path()
      .projection(projection);

    wa = svg({
      id: "wwu-wa-admissions-map",
      source: Drupal.settings.wwu_admissions_map.wa,
      parent: "#wwu-admissions-map",
      width: width,
      height: height,
      path: path,
      callback: function (paths) {
        paths
          .attr("id", function (d) { return d.properties.name; })
          .attr("class", function (d) { return "county " + d.properties.name; })
          .on("click", function (d) { clickFeature(d, path, wa); });
      }
    });

    button('#wwu-admissions-button', function () {
      wa.transition()
        .duration(timeout)
        .style("opacity", "0")
        .each("end", function () {
          wa.remove();
          usCounselors();
        });
    });
  }

  /**
   * Center the given SVG element on the bounding box of the active feature
   * (e.g. by mouse click). Applies a transformation animation with timeout
   * duration.
   *
   * @param {Object}
   *          d the d3 data binding object for the active feature
   * @param {Object}
   *          path the d3 path object to be applied
   * @param {Object}
   *          svg the parent SVG element for the clicked feature
   * @param {Function}
   *          [callback] callback to apply once the animation is complete
   */
   function zoomToFeature(d, path, svg, callback) {
     var bounds,
         dx,
         dy,
         height,
         scale,
         translate,
         width,
         x,
         y;

     bounds = path.bounds(d);
     dx = bounds[1][0] - bounds[0][0];
     dy = bounds[1][1] - bounds[0][1];
     x = (bounds[0][0] + bounds[1][0]) / 2;
     y = (bounds[0][1] + bounds[1][1]) / 2;
     height = parseInt(svg.style("height"));
     width = parseInt(svg.style("width"));
     scale = 0.9 / Math.max(dx / width, dy / height);
     translate = [width / 2 - scale * x, height / 2 - scale * y];

     svg.select("g").transition()
       .duration(timeout)
       .style("stroke-width", 3 / scale + "px")
       .attr("transform", "translate(" + translate + ")scale(" + scale + ")")
       .each("end", function () {
         if (typeof callback === 'function') {
           callback();
         }
       });
  }

  /**
   * Main bootstrap function. Perform setup tasks, then attempt to display the
   * US counselors map.
   */
  Drupal.behaviors.wwuAdmissionsMap = {
    attach: function (context, settings) {
      timeout = 750;

      /**
       * Store the JSON data that relates counselors to their respective regions
       * in the counselors variable. The data is first validated and if invalid
       * data is detected, an error is logged and the map is not drawn.
       */
      $.getJSON(Drupal.settings.wwu_admissions_map.counselors, function (data) {
        var properties;

        counselorsJSON = data;
        properties = [
          "image",
          "name",
          "phone",
          "email",
          "profile",
          "region"
        ];

        try {
          validateViewsJson(counselorsJSON, properties);
          setupOverlay();
          usCounselors();
        } catch (error) {
          displayRuntimeError(error);
        }
      })
        .fail(function (jqXHR, textStatus, error) {
          displayJsonError(Drupal.settings.wwu_admissions_map.counselors);
        });
    }
  };

})(jQuery, Drupal, this, this.document);
