/**
 * @file mapDynamics.js
 * @author Jacob Shafer : WWU Office of Admissions: Western Washington University
 * November 2015 - January 2016
 */
(function ($, Drupal, window, document, undefined) {
   var baseurl = 'https://admissions.wwu.edu/counselor/';
   var countryData = new Map();
   var washingtonData = new Map();

   var debug = false;
   if (debug) {
      alert('jQuery is working!');
   }

   /**
    * Display a visible error message in the browser when a JSON file fails to load.
    * @param {String} file : path to the document that failed to load
    */
   function displayJsonError(file) {
      displayRuntimeError("The JSON file \"" + file + "\" failed to load.");
   }

   /**
    * Display a general error message to the user with the given text.
    * @param {String} message : error message text
    */
   function displayRuntimeError(message) {
      $("#wwu-admissions-error").text(message).show();
   }

   /**
    * Gets and returns the top-level keys of an object.
    * @param {Object} object : the object to scan for parameters
    * @returns {String[]} the array of keys
    */
   function getObjectKeys(object) {
      var keys = [];
      for (var key in object) {
         keys[keys.length] = key;
      }
      return keys;
   }

   /**
    * Check that the correct keys and data types are provided by the counselors
    * JSON view. If errors are found, attempt to provide guidance to correct.
    * @param {Object} data : the JSON data object
    * @param {String[]} requiredKeys : array of required keys
    * @throws {Error} : if a key (field) is not found in the given dataset.
    */
   function validateViewsJson(data, requiredKeys) {
      var keys = getObjectKeys(data[0]);
      for (var i = requiredKeys.length; i--;) {
         var key = requiredKeys[i];
         if (keys.indexOf(key) < 0) {
            throw ("The field '" + key + "' was not found in the JSON view '" +
               Drupal.settings.wwu_admissions_map.counselors + "' required by this module. " +
               "Ensure that the view is present and configured correctly.");
         }
      }
   }

   /**
    * Add the overlay background and container to the DOM in the appropriate locations.
    */
   function setupOverlay() {
     $('<div class="wwu-admissions-overlay-container"></div>').prependTo('body');
     $('<div class="wwu-admissions-overlay-background"></div>').prependTo('body');
   }

   /**
    * Finds the counselor for the entity that was clicked on, and brings up overlay if found
    * @param {object} entity : the svg element that was clicked on
    */
   function getCounselor(entity) {
      var region = entity.attr('class').split(' ')[2];
      var counselor;
      var WashingtonClicked = false;

      if (region === 'West') {
         var state = entity.attr('id');
         if (state === undefined) {
            state = entity.parent().attr('id'); /* for alaska and hawaii */
         }
         if (state === 'Washington') {
            WashingtonClicked = true;
         } else {
            counselor = countryData.get(state);
            if (counselor === undefined) {
               displayRuntimeError('no counselor mapped for ' + state);
               return;
            }
         }
      } else {
         counselor = countryData.get(region);
         if (counselor === undefined) {
            displayRuntimeError('no counselor mapped for ' + region);
            return;
         }
      }
      displayOverlay(WashingtonClicked, counselor);
   }

   /**
    * Displays the overlay with the data stored in counselor
    * @param {boolean} WashingtonClicked : true if washington state was clicked
    * @param {object} counselor : object containing counselor data
    */
   function displayOverlay(WashingtonClicked, counselor) {
      // grab the container and clear it from the previous use
      var container = $('.wwu-admissions-overlay-container').empty();
      var background = $('.wwu-admissions-overlay-background');
      function showMap() {
         $('#SchoolWebForm').hide().appendTo($('body'));
         background.unbind().fadeOut();
         container.fadeOut();
      }
      background.on('click', showMap);
      /* Create a copy of the empty overlay and remove its attribute */
      if (WashingtonClicked) {
         var overlay = $('#wwu-admissions-overlay-form').clone().removeAttr('id');
         overlay.appendTo(container);
         $('#SchoolWebForm').appendTo(overlay).show();
      } else {
         fillOverlay(counselor).appendTo(container);
      }
      background.fadeIn();
      container.fadeIn();
      $('.close-overlay').on('click', showMap);
   }

   /**
   * Fills the overlay template with counselor information
   * @param {object} counselor : object containing a counselor's information
   * @returns {object} : jquery object containing a data-filled overlay
   */
   function fillOverlay(counselor) {
      var overlay = $('#wwu-admissions-overlay-template').clone().removeAttr('id');
      overlay.find('.wwu-admissions-overlay-name-content')
         .text(counselor.name);
      overlay.find('.wwu-admissions-overlay-phone-content')
         .text(counselor.phone).attr("href", 'tel:+' + counselor.phone);

      overlay.find('.wwu-admissions-overlay-email-content')
         .attr('href', 'mailto:' + counselor.email)
         .text(counselor.email);
      overlay.find('.wwu-admissions-overlay-image-content')
         .attr('src', counselor.image);
      var names = counselor.name.toLowerCase().split(' ');
      overlay.find('.wwu-admissions-overlay-profile-content')
         .attr('href', baseurl + names[0] + '-' + names[1])
         .text('Get to know ' + counselor.name + ' better!');
      return overlay;
   }

   /**
    * Associate each svg region with its counselor object
    * @param {object} stateCounselors : json received from drupal view
    */
   function bindStateCounselors(stateCounselors) {
      /* Note: all svg elements have three classes:
         1) stX : where x is used to color the state
         2) 'state'
         3) region
      */
      $('.state').each(function () {
         var region = $(this).attr('class').split(' ')[2];
         for (var entry in stateCounselors) {
            var storedRegion = stateCounselors[entry].region;
            if (region === 'West') {
               var state = $(this).attr('id');

               // Alaska and Hawaii are made up of groups, so sub-units don't have state ids
               if (state === undefined) {
                  state = storedRegion;
               }
               if (countryData.get(state) === undefined) {
                  // IDs can't have spaces: i.e. NewMexico vs New Mexico
                  if (state === storedRegion.replace(/\s/g, '')) {
                     countryData.set(state, stateCounselors[entry]);
                  }
               }
            /* Don't overwrite entries; note view strings suffixed with 'U.S. '*/
         } else if (countryData.get(region) === undefined && region + ' U.S.' === storedRegion) {
               countryData.set(region, stateCounselors[entry]);
            }
         }
      }).on('click', function () {
         getCounselor($(this));
      });

      setupSpecialButtons(stateCounselors);
   }

   /**
    * Associate intl-student and living-abroad buttons with counselor objects
    * @param {object} stateCounselors : json received from drupal view
    */
   function setupSpecialButtons(stateCounselors) {
      var intl = 'International Students';
      var abroad = 'U.S. Territories and U.S. Citizens Living Abroad';

      // Declare functions outside of loop
      var loadITNL = function () {
         displayOverlay(false, countryData.get(intl));
      };
      var loadABROAD = function () {
         displayOverlay(false, countryData.get(abroad));
      };

      // Setup the mappings for the two buttons
      for (var entry in stateCounselors) {
         var storedRegion = stateCounselors[entry].region;
         if (countryData.get(storedRegion) === undefined) {
            countryData.set(storedRegion, stateCounselors[entry]);
            if (storedRegion === intl) {
               $('#intl-student').on('click', loadITNL);
            } else if (storedRegion === abroad) {
               $('#living-abroad').on('click', loadABROAD);
            }
         }
      }
   }

   /**
    * Bind Washington State Schools to counselors for search form overlay
    * @param {Object} washingtonCounselors : json data received from drupal view
    */
   function setupHighSchoolSearch(washingtonCounselors) {
      var schools = [];
      for (var entry in washingtonCounselors) {
         var entity = washingtonCounselors[entry];
         schools.push(entity.institution);
         washingtonData.set(entity.institution, entity.counselor);
      }
      $('#SchoolChoices').autocomplete({
         source: function(request, response) {
            var results = $.ui.autocomplete.filter(schools, request.term);
            response(results.slice(0, 3));
            $('.ui-autocomplete').off('menufocus hover mouseover mouseenter');
         }
      });

      // add callback to submit button
      $('#SchoolWebForm > form').submit(function (e) {
         e.preventDefault();
         var school = $('#SchoolChoices').val();
         if (school.length > 0) {
            var name = washingtonData.get(school);
            loadProfilePage(name);
         }
      });
   }

   /**
   * Loads the profile page of the counselor with the given name
   *  - shows alert message if the given name can't be split into first and last
   * @param {String} name : the name of a counselor
   */
   function loadProfilePage(name) {
      var names = name.toLowerCase().split(' ');
      if (names[1] === undefined) {
         alert("No last name associated with couselor: " + name);
      } else {
         // Take the user to the counselor's own page
         window.location.href = baseurl + names[0] + "-" + names[1];
      }
   }

   /**
   * Applies event listeners to western states and the 3 other regions as units
   *  hover -> brighter
   * @param {integer} intensity : how bright the increase should be
   */
   function addColorDynamics(intensity) {
      // Each eastern region should highlight as a unit
      applyToRegion('.Midwest', intensity);
      applyToRegion('.Northeast', intensity);
      applyToRegion('.Southeast', intensity);

      // same thing with all the parts of Alaska and Hawaii
      applyToRegion('#Alaska > .West', intensity);
      applyToRegion('#Hawaii > .West', intensity);

      // all other western states treate individually
      $('svg > .West').on('mouseenter', function () {
         updateColor($(this), intensity);
      }).on('mouseleave', function () {
         updateColor($(this), -1 * intensity);
      });
   }

   /**
   * Applies the color-modulation event listeners to a region as a whole
   * @param {String} region : the class name for the region
   * @param {integer} intensity : how bright the increase should be
   */
   function applyToRegion(region, intensity) {
      $(region).on('mouseenter', function () {
         $(region).each(function () {
            updateColor($(this), intensity);
         });
      }).on('mouseleave', function () {
         $(region).each(function () {
            updateColor($(this), -1 * intensity);
         });
      });
   }

   /** Changes an elements fill color property
   * @param {Jquery selection} element : the element whose fill will be modified
   * @param {integer} change : value added to current r,g,b
   * @throws {Error} : if for some reason the regular expression doesn't work
   *     Note: if the regex doesn't make sense: plug it into regex tester such as regexr.com
   */
   function updateColor(element, change) {
      try {
         // regex to match 'rgb(x, y, z)'
         var rgb = element.css('fill').match(/^rgb\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)$/);
         var red = parseInt(rgb[1]);
         var green = parseInt(rgb[2]);
         var blue = parseInt(rgb[3]);
         var newRGB = ("rgb(" + (red+change) + ", " + (green+change) + ", " + (blue+change) + ")");
         element.css('fill', newRGB);
      } catch (err) {
         if (debug) {
            displayRuntimeError(err);
         }
      }
   }

   /**
     * Main bootstrap function. Perform setup tasks, then binds the data to the map
     * Error handlers are commented out for production
     */
   Drupal.behaviors.wwuAdmissionsMap = {
      attach: function (context, settings) {
         setupOverlay();
         addColorDynamics(40.0);

         $.getJSON(Drupal.settings.wwu_admissions_map.CountryCounselors, function (data) {
            var properties = ["image", "name", "phone", "email", "profile", "region"];

            try {
               validateViewsJson(data, properties);
               bindStateCounselors(data);
            } catch (error) {
               if (debug) {
                  displayRuntimeError(error);
               }
            }
         }).fail(function (jqXHR, textStatus, error) {
            if (debug) {
               displayJsonError(Drupal.settings.wwu_admissions_map.CountryCounselors);
            }
         });

         $.getJSON(Drupal.settings.wwu_admissions_map.WashingtonCounselors, function (data) {
            var properties = ["type", "institution", "county", "code", "region", "counselor"];

            try {
               validateViewsJson(data, properties);
               setupHighSchoolSearch(data);
            } catch (error) {
               if (debug) {
                  displayRuntimeError(error);
               }
            }
         }).fail(function (jqXHR, textStatus, error) {
            if (debug) {
               displayJsonError(Drupal.settings.wwu_admissions_map.WashingtonCounselors);
            }
         });
      }
   };
})(jQuery, Drupal, this, this.document);