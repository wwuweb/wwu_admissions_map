<noscript>
   <meta http-equiv="refresh" content="0;url=counselors" />
</noscript>
<div id="wwu-admissions-map">
   <div id="wwu-admissions-error" class="messages error"></div>
</div>
<div id="counselorMap">
      <?php
         require_once('usMap.svg.tpl.php');
      ?>
</div>
<div id="wwu-admissions-overlay-form" class="wwu-admissions-overlay">
</div>
<div id="SchoolWebForm">
   <h2>Washington School Search</h2>
   <form>
      <div class="ui-widget">
         <label for="SchoolChoices">Enter High School or College:</label>
         <p>(If Running Start, enter High School)</p>
         <input type="text" id="SchoolChoices">
         <input type="submit" id="SchoolFound" value="Submit">
      </div>
   </form>
   <button class="close-overlay">
      <img alt="x" src="<?php echo $close_overlay_button; ?>">
   </button>
</div>
<div id="wwu-admissions-overlay-template" class="wwu-admissions-overlay">
  <div class="wwu-admissions-overlay-image-wrapper wwu-admissions-overlay-field-wrapper">
    <img class="wwu-admissions-overlay-image-content" width="200" height="200">
  </div>
  <div class="wwu-admissions-overlay-name-wrapper wwu-admissions-overlay-field-wrapper">
    <span class="wwu-admissions-overlay-name-content wwu-admissions-overlay-field"></span>
  </div>
  <div class="wwu-admissions-overlay-phone-wrapper wwu-admissions-overlay-field-wrapper">
    <a href="" class="wwu-admissions-overlay-phone-content wwu-admissions-overlay-field"></a>
  </div>
  <div class="wwu-admissions-overlay-email-wrapper wwu-admissions-overlay-field-wrapper">
    <a class="wwu-admissions-overlay-email-content wwu-admissions-overlay-field"></a>
  </div>
  <div class="wwu-admissions-overlay-profile-wrapper wwu-admissions-overlay-field-wrapper">
    <a class="wwu-admissions-overlay-profile-content wwu-admissions-overlay-field"></a>
  </div>
  <button class="close-overlay">
     <img alt="x" src="<?php echo $close_overlay_button; ?>">
  </button>
</div>
